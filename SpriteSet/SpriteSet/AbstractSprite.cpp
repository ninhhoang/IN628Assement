#include "AbstractSprite.h"
#include "TileMap.h"

AbstractSprite::AbstractSprite(Graphics^ startCanvas,
	array<Bitmap^>^ startSpriteSheet,
	int startNFrames,
	Random^ startRandom,
	int startWorldWidth,
	int startWorldHeight,
	int startXpos,
	int startYpos,
	TileMap^ startTileMap)
{
	canvas = startCanvas;
	spriteSheet = startSpriteSheet;
	nFrames = startNFrames;
	rand = startRandom;
	worldWidth = startWorldWidth;
	worldHeight = startWorldHeight;
	xPos = startXpos;
	yPos = startYpos;
	currentFrame = 0;
	tileMap = startTileMap;
	frameWidth = (int)spriteSheet[0]->Width / nFrames;
	frameHeight = (int)spriteSheet[0]->Height;
	walkable = false;
}

void AbstractSprite::CheckWalkable(int worldRow1, int worldColume1, int worldRow2, int worldColume2)
{
	bool walkable1 = tileMap->GetTileWalkability(worldRow1, worldColume1);
	bool walkable2 = tileMap->GetTileWalkability(worldRow2, worldColume2);

	if (walkable1 == true && walkable2 == true)
	{
		walkable = true;
	}
	else
	{
		walkable = false;
	}
}

void AbstractSprite::SetRandomLocation()
{

}

void AbstractSprite::SetRandomVelocities()
{

}

void AbstractSprite::Move()
{

}

void AbstractSprite::UpdateFrame()
{
	currentFrame = (currentFrame + 1) % nFrames;
}

void AbstractSprite::SetSpriteSheet(Bitmap^ newSpriteSheet, int newNFrames)
{

}

