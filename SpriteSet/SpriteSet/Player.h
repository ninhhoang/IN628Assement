#pragma once
#include "CombatSprite.h"
#include "PelletList.h"

ref class Player : public CombatSprite
{
public:
	PelletList^ pelletList;

	Player(Graphics^ startCanvas,
		array<Bitmap^>^ startSpriteSheet,
		int startNFrames,
		Random^ startRandomn,
		int startWorldWidth,
		int startWorldHeight,
		int startXpos,
		int startYpos,
		int startSpeed,
		TileMap^ startTileMap);
	
	void Shoot();
	void PelletSequence();
	void Move();
	int GetSpriteWidth();
};

