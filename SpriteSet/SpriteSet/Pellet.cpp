#include "Pellet.h"

Pellet::Pellet(int startxPos, int startyPos, Graphics^ startCanvas, Random^ startRand,
	int startXDirection,
	int startyDirection)
{
	xPos = startxPos;
	yPos = startyPos;
	canvas = startCanvas;
	rand = startRand;
	xDirection = startXDirection;
	yDirection = startyDirection;
	int red = rand->Next(COLOR_BIT);
	int green = rand->Next(COLOR_BIT);
	int blue = rand->Next(COLOR_BIT);
	pelletColor = Color::FromArgb(ALPHA_BIT, red, green, blue);
	isAlive = true;
	collision = false;
	brush = gcnew SolidBrush(pelletColor);
	yVel = rand->Next(MIN_SPEED, MAX_SPEED + 1);
	xVel = rand->Next(MIN_SPEED, MAX_SPEED + 1);

	initXpos = xPos;
	initYpos = yPos;
}

void Pellet::Draw()
{
	canvas->FillEllipse(brush, xPos, yPos, PELLET_DIA, PELLET_DIA);
}

void Pellet::Move()
{
	xPos = xPos + xVel * xDirection;
	yPos = yPos + yVel * yDirection;
	canvas->FillEllipse(brush, xPos, yPos, PELLET_DIA, PELLET_DIA);

	int travelDisX = initXpos - xPos;
	int travelDisY = initYpos - yPos;
	 
	if (travelDisX < 0)
	{
		travelDisX = travelDisX * -1;
	}
	if (travelDisY < 0)
	{
		travelDisY = travelDisY * -1;
	}

	if (travelDisX > PELLET_RANGE || travelDisY > PELLET_RANGE)
	{
		isAlive = false;
	}
}

void Pellet::setCollision(bool startcollision)
{
	collision = startcollision;
}