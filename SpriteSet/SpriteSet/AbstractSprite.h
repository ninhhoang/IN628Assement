#pragma once
#include "TileMap.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

ref class AbstractSprite abstract
{
protected:
	Graphics^ canvas;
	array<Bitmap^>^ spriteSheet;
	Random^ rand;
	TileMap^ tileMap;
	int currentFrame;
	int nFrames;
	int xVel;
	int yVel;
	int worldWidth;
	int worldHeight;

public:
	
	property int frameWidth;
	property int frameHeight;
	property int xPos;
	property int yPos;
	property bool IsAlive;
	property bool walkable;

public: 

	AbstractSprite(Graphics^ startCanvas,
		array<Bitmap^>^ startSpriteSheet,
		int startNFrames,
		Random^ startRandom,
		int startWorldWidth,
		int startWorldHeight,
		int startXpos,
		int startYpos,
		TileMap^ startTileMap);

	void CheckWalkable(int worldRow, int worldColume, int worldRow2, int worldColume2);
	void SetRandomLocation();
	void SetRandomVelocities();
	virtual void Draw() abstract;	// Use this overload to change drawn size of frame: DrawImage(spriteSheet, displayRect, copyRect, GraphicsUnit::Pixel);
	void Move();
	void UpdateFrame();
	void SetSpriteSheet(Bitmap^ newSpriteSheet, int newNFrames);
};

