#include "TileMap.h"


TileMap::TileMap(TileManager^ startTileManager, Graphics^ startCanvas, int startNRows, int StartNCols)
{
	tileManager = startTileManager;
	canvas = startCanvas;
	nRows = startNRows;
	nCols = StartNCols;
	map = gcnew array<int, 2>(startNRows, StartNCols);

	//get tile size
	Bitmap^ temp = tileManager->GetTileBitmap(12);
	size = temp->Width;
}

void TileMap::SetMapEntry(int row, int col, int tileIndex)
{
	map[row, col] = tileIndex;
}

int TileMap::GetMapEntry(int row, int col)
{
	return map[row, col];
}

void TileMap::DrawMap()
{
	for (int row = 0; row < nRows; row++)
	{
		for (int col = 0; col < nCols; col++)
		{
			//get integer
			int entry = GetMapEntry(row, col);

			//calculate position
			int xPos = col * size;
			int yPos = row * size;

			//draw tile
			Bitmap^ entryBitmap = tileManager->GetTileBitmap(entry);
			Rectangle rec = Rectangle(xPos, yPos, size, size);
			canvas->DrawImage(entryBitmap, rec);
		}
	}
}

Bitmap^ TileMap::GetMapCellBitmap(int worldRow, int worldColume)
{
	int mapValue = map[worldRow, worldColume];
	Bitmap^ tileImage = tileManager->GetTileBitmap(mapValue);
	return tileImage;
}

void TileMap::LoadMap(String^ mapFileName)
{
	//initialize streamreader
	StreamReader^ sr = File::OpenText(mapFileName);
	String^ currentLine = "";

	//declare temp string array
	array<String^>^ index = gcnew array<String^>(nCols);

	//use StreamReader loop to fill map array
	int rowCounter = 0;
	int limit = 0;
	while (currentLine = sr->ReadLine())
	{
		limit++;
		if (limit < LIMITER)
		{
			//split
			index = currentLine->Split(',');
			for (int i = 0; i < nCols; i++)
			{
				//convert string to int assign value to map
				map[rowCounter, i] = Convert::ToInt32(index[i]);
			}
			rowCounter++;
		}
	}
}

bool TileMap::GetTileWalkability(int worldRow, int worldColume)
{
	int mapValue = map[worldRow, worldColume];
	bool walkable = tileManager->GetTileWalkable(mapValue);
	return walkable;
}
