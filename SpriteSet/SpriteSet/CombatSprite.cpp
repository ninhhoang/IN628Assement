#include "CombatSprite.h"

CombatSprite::CombatSprite(Graphics^ startCanvas,
	array<Bitmap^>^ startSpriteSheet,
	int startNFrames,
	Random^ starRandom,
	int startWorldWidth,
	int startWorldHeight,
	int startXpos,
	int startYpos,
	int startSpeed,
	TileMap^ startTileMap)
	: AbstractSprite(startCanvas,
		startSpriteSheet,
		startNFrames,
		starRandom,
		startWorldWidth,
		startWorldHeight,
		startXpos,
		startYpos,
		startTileMap)
{
	speed = startSpeed;
	spriteDirection = EDirection::EAST;
	creatureState = ECreatureState::WANDERING;

	velocityDirection = gcnew array<Point>(MAX_DIRECTION);
	velocityDirection[0] = Point(1, 0);
	velocityDirection[1] = Point(0, 1);
	velocityDirection[2] = Point(-1, 0);
	velocityDirection[3] = Point(0, -1);
}

void CombatSprite::Draw()
{
	int startX = currentFrame * frameWidth;
	int startY = 0;

	Rectangle rec = Rectangle(startX, startY, frameWidth, frameHeight);
	currentBitmap = spriteSheet[spriteDirection];
	canvas->DrawImage(currentBitmap, xPos, yPos, rec, GraphicsUnit::Pixel);
}

void CombatSprite::ChangeRandomDirection()
{

}

double CombatSprite::ComputeDistance(AbstractSprite^ aSprite)
{
	return 0;
}

/*AbstractSprite^ FindNearIndex(array<AbstractSprite^>^ otherGuys)
{
	return 0;
}*/

void CombatSprite::UpdateState(array<AbstractSprite^>^ food, array<AbstractSprite^>^ obstacles)
{

}

void CombatSprite::PerformAction()
{

}

