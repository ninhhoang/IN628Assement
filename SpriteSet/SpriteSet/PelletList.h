#include"Pellet.h"
#include "TileMap.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
#pragma once

ref class PelletList
{
private:
	TileMap^ tileMap;
	int frameWidth;
	Pellet^ head;
	Pellet^ tail;
public:
	PelletList(TileMap^ starttileMap, int startFrameWidth);
	void addPellet(Pellet^ newPellet);
	void deleteOnePellet(Pellet^ deletePellet);
	void deleteDeadPellet();
	void movePellets();
	void drawPellet();
	int countPellets();
	void CheckWallCollision();
};

