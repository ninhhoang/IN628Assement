#pragma once
# include "AbstractSprite.h"

#define MAX_DIRECTION 4

public enum ECreatureState { REST, TARGETING, WANDERING, ATTACKING };
public enum  EDirection { EAST, SOUTH, WEST, NORTH };

ref class CombatSprite : public AbstractSprite
{
protected:
	const int RESTING_TIME = 50;
	const int WANDERING_LIMIT = 100;
	const int WANDERING_MINIMUM = 10;

	Bitmap^ currentBitmap;
	int restingTicks;
	int wanderingTime;
	int wanderingTicks;

	int sensingRange;	
	int speed;

	array<Point>^ velocityDirection;

public:

	ECreatureState creatureState;
	EDirection spriteDirection;

	CombatSprite(Graphics^ startCanvas,
		array<Bitmap^>^ startSpriteSheet,
		int startNFrames,
		Random^ startRandom,
		int startWorldWidth,
		int startWorldHeight,
		int startXpos,
		int startYpos,
		int startSpeed,
		TileMap^ startTileMap);

	virtual void Draw() override;

	void ChangeRandomDirection();

	double ComputeDistance(AbstractSprite^ aSprite);

	//AbstractSprite^ FindNearIndex(array<AbstractSprite^>^ otherGuys);

	void UpdateState(array<AbstractSprite^>^ food, array<AbstractSprite^>^ obstacles);

	void PerformAction();
};

