#include "TileManager.h"


TileManager::TileManager(int startNtitle)
{
	nTile = startNtitle;
	tileArray = gcnew array<Tile^>(nTile);
}

Bitmap^ TileManager::GetTileBitmap(int tileIndex)
{
	return tileArray[tileIndex]->tileBitmap;
}

void TileManager::SetTileArray(int tileIndex, Tile^ tileToEnter)
{
	tileArray[tileIndex] = tileToEnter;
}

bool TileManager::GetTileWalkable(int tileIndex)
{
	return tileArray[tileIndex]->GetWalkable();
}
