#include "PelletList.h"

PelletList::PelletList(TileMap^ startTileMap, int startFrameWidth)
{
	tail = nullptr;
	head = nullptr;
	frameWidth = startFrameWidth;
	tileMap = startTileMap;
}

void PelletList::CheckWallCollision()
{
	Pellet^ nodeWalker = head;
	while (nodeWalker != nullptr)
	{
		int xPos = nodeWalker->xPos;
		int yPos = nodeWalker->yPos;

		int viewportTileX = xPos / frameWidth;
		int viewportTileY = yPos / frameWidth;

		bool collision = tileMap->GetTileWalkability(viewportTileX, viewportTileY);
		nodeWalker->setCollision(collision);
	}
}

void PelletList::addPellet(Pellet^ newPellet)
{
	if (head == nullptr)
	{
		head = newPellet;
		tail = newPellet;
	}
	else
	{
		tail->Next = newPellet;
		tail = newPellet;
	}
	tail->Next = nullptr;
}

void PelletList::deleteOnePellet(Pellet^ deletePellet)
{
	Pellet^ nodeWalker;
	Pellet^ nodeFollower;
	nodeWalker = head;

	if (head == deletePellet)
	{
		if (countPellets() == 1)
		{
			head = nullptr;
			tail = nullptr;
		}
		else
		{
			head = head->Next;
		}
	}
	else
	{
		while (nodeWalker != deletePellet)
		{
			nodeFollower = nodeWalker;
			nodeWalker = nodeWalker->Next;
		}
		if (nodeWalker == tail)
		{
			tail = nodeFollower;
		}
		else
		{
			nodeFollower->Next = nodeWalker->Next;
		}
		delete nodeWalker;
	}
}

void PelletList::deleteDeadPellet()
{
	Pellet^ nodeWalker = head;
	while (nodeWalker != nullptr)
	{
		bool check = nodeWalker->getAlive();
		bool collistion = nodeWalker->getCollision();
		if (check == false )
		{
			deleteOnePellet(nodeWalker);
		}
		nodeWalker = nodeWalker->Next;
	}
}

void PelletList::movePellets()
{
	Pellet^ nodeWalker = head;
	while (nodeWalker != nullptr)
	{
		nodeWalker->Move();
		nodeWalker = nodeWalker->Next;
	}
}

void PelletList::drawPellet()
{
	Pellet^ nodeWalker = head;
	while (nodeWalker != nullptr)
	{
		nodeWalker->Draw();
		nodeWalker = nodeWalker->Next;
	}
}

int PelletList::countPellets()
{
	Pellet^ nodeWalker = head;
	int i = 0;
	while (nodeWalker != nullptr)
	{
		i++;
		nodeWalker = nodeWalker->Next;
	}
	return i;
}