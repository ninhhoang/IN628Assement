#pragma once
#include "TileMap.h"
#include "TileManager.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

#define NROW 20
#define NCOL 50

ref class Viewport
{
private:
	int viewportTileWide;
	int viewportTileHigh;
	int tileSize;
	int viewportXWide;
	int viewportYWide;
	int spriteWidth;

	TileMap^ backgroundMap;
	Graphics^ canvas;

public:
	property int ViewportWorldX;
	property int ViewportWorldY;

	Viewport(int startX, int startY, int startTileWide, int startTileHigh, TileMap^ startBackgroundMap, Graphics^ startCanvas, int startXWide, int startYWide, int startSpriteWidth);

	void ViewportMove(int xMove, int yMove);
	void ViewportDraw();
	void MoveWithPlayer(int playX, int playerY);
};