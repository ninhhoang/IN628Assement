using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
#pragma once

#define PELLET_DIA 10
#define PELLET_SPEED 10
#define MAX_SPEED 50
#define MIN_SPEED 30
#define COLOR_BIT 256
#define ALPHA_BIT 200
#define TOP_BOUND 10
#define LEFT_BOUND 0
#define RIGHT_BOUND 522
#define BOT_BOUND 708
#define PELLET_RANGE 300

ref class Pellet
{
private:
	int diameter;
	int xVel;
	int yVel;
	int randomColor;
	bool isAlive;
	Color pelletColor;
	Graphics^ canvas;
	Brush^ brush;
	Random^ rand;

	int xDirection;
	int yDirection;
	int initXpos;
	int initYpos;
	bool collision;

public:
	Pellet^ Next;

	property int yPos;
	property int xPos;

	Pellet(int startxPos, int startyPos, Graphics^ startCanvas, Random^ startRand,
	int startxDirection,
	int startyDirection);

	void Draw();
	void Move();
	void setCollision(bool startcollision);
	bool getAlive() { return isAlive; }
	bool getCollision() { return collision; }
};

