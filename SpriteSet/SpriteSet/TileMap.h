#pragma once
#include "TileManager.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::IO;

#define LIMITER 20
ref class TileMap
{
private:
	TileManager^ tileManager;
	array<int, 2>^ map;
	Graphics^ canvas;
	int nRows;
	int nCols;
	int size;

public:

	TileMap(TileManager^ startTileManager, Graphics^ startCanvas, int startNRows, int StartNCols);
	void SetMapEntry(int row, int col, int tileIndex);
	int GetMapEntry(int row, int col);
	void DrawMap();
	Bitmap^ GetMapCellBitmap(int worldRow, int wordlColume);
	void LoadMap(String^ mapFileName);
	bool GetTileWalkability(int worldRow, int wordlColume);
};

