#pragma once
#include "Tile.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

ref class TileManager
{
private:
	array<Tile^>^ tileArray;
	int nTile;

public:

	TileManager(int startNTitle);

	Bitmap^ GetTileBitmap(int tileIndex);

	void SetTileArray(int tileIndex, Tile^ tileToEnter);

	bool GetTileWalkable(int tileIndex);
};

