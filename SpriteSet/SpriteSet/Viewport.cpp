#include "Viewport.h"

Viewport::Viewport(int startX, int startY, int startTileWide, int startTileHigh, TileMap^ startBackgroundMap, Graphics^ startCanvas, int startXWide, int startYWide, int startSpriteWidth)
{
	//get value 
	viewportTileWide = startTileWide;
	viewportTileHigh = startTileHigh;
	backgroundMap = startBackgroundMap;
	canvas = startCanvas;
	viewportXWide = startXWide;
	viewportYWide = startYWide;
	spriteWidth = startSpriteWidth;
	//set tile size
	Bitmap^ temp = backgroundMap->GetMapCellBitmap(0, 0);
	tileSize = temp->Width;
}

void Viewport::ViewportMove(int xMove, int yMove)
{
	//temp condition
	if (ViewportWorldX + (viewportTileWide * tileSize) < tileSize * NCOL)
	{
		ViewportWorldX += xMove;
		ViewportWorldY += yMove;
	}
}

void Viewport::ViewportDraw()
{
	//to know which tile the viewport is starting on
	int viewportTileX = ViewportWorldX / tileSize;
	int viewportTileY = ViewportWorldY / tileSize;

	//draw tile
	for (int r = viewportTileX; r <= (viewportTileX + viewportTileWide); r++)
	{
		for (int c = viewportTileY; c <= (viewportTileY + viewportTileHigh); c++)
		{
			//set up position of each tile on the viewport
			int screenX = ((r - viewportTileX) * tileSize);
			int screenY = ((c - viewportTileY) * tileSize);

			//set up offset
			int viewportOffsetX = ViewportWorldX % tileSize;
			int viewportOffsetY = ViewportWorldY % tileSize;

			//apply offset to screen position
			screenX -= viewportOffsetX;
			screenY -= viewportOffsetY;

			//draw
			Bitmap^ entryBitmap = backgroundMap->GetMapCellBitmap(r, c);
			Rectangle rec = Rectangle(screenX, screenY, tileSize, tileSize);
			canvas->DrawImage(entryBitmap, rec);
		}
	}
}

void Viewport::MoveWithPlayer(int playerX, int playerY)
{
	ViewportWorldX = playerX;

	if (ViewportWorldX < 0)
	{
		ViewportWorldX = 0;
	}

	if ((playerY - ViewportWorldY) < viewportYWide / 2)
	{
		ViewportWorldY = 0;
	}
	else
	{
	}
}
