#pragma once
#include "TileMap.h"
#include "TileManager.h"
#include "Tile.h"
#include "Player.h"
#include "Viewport.h"

namespace SpriteSet {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MainForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{
	public:
		MainForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Timer^  timer1;
	protected:
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Interval = 50;
			this->timer1->Tick += gcnew System::EventHandler(this, &MainForm::timer1_Tick);
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(786, 489);
			this->Name = L"MainForm";
			this->Text = L"MainForm";
			this->Load += gcnew System::EventHandler(this, &MainForm::MainForm_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &MainForm::MainForm_KeyDown);
			this->ResumeLayout(false);

		}
#pragma endregion

#define NTITLE 13
#define NROW 20
#define NCOL 50
#define CHICKEN_START_POS 70
#define WORLD_WIDTH 1000

		//global variables
		Graphics^ canvas;
		Random^ rand;
		Rectangle rec;

		//object
		TileMap^ tileMap;
		TileManager^ tileManager;
		Player^ player;

		//for double buffering
		Bitmap^ offScreenBitmap;
		Graphics^ offScreenCanvas;
		Viewport^ viewport;

	private: System::Void MainForm_Load(System::Object^  sender, System::EventArgs^  e) 
	{
		//initialize variables for double buffering
		offScreenBitmap = gcnew Bitmap(this->Width, this->Height);
		offScreenCanvas = Graphics::FromImage(offScreenBitmap);
		//create graphics
		canvas = CreateGraphics();

		//initialize rand
		rand = gcnew Random;
		// make bitmap		

		//initialize tileMap
		tileMap = InitializeTileMap();

		//initialize chicken sprite
		player = GeneratePlayer();

		//initialize viewport
		int spriteWidth = GetSpriteSize(player);
		viewport = gcnew Viewport(0, 0, 10, 7, tileMap, offScreenCanvas, this->Width, this->Height, spriteWidth);

		//initialize viewport position
		viewport->ViewportWorldX = 0;
		viewport->ViewportWorldX = 0;

		//fill tileMap
		tileMap->LoadMap("../maze.txt");
		timer1->Enabled = true;
	}

	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) 
	{
		player->Move();
		viewport->ViewportDraw();
		player->Draw();
		player->PelletSequence();
		player->UpdateFrame();
		canvas->DrawImage(offScreenBitmap, 0, 0);
	}

	private: Void RandomMap()
	{
		//randomly fill tileMap with integer
		for (int row = 0; row < NROW; row++)
		{
			for (int col = 0; col < NCOL; col++)
			{
				int typeTile = rand->Next(3);
				tileMap->SetMapEntry(row, col, typeTile);
			}
		}
	}

	private: Player^ GeneratePlayer()
	{
		//set value for some initial variables
		int framesOfChicken = 8;
		int direction = 4;

		//get bitmap
		Bitmap^ east = gcnew Bitmap("../Chicken East.bmp");
		Bitmap^ south = gcnew Bitmap("../Chicken South.bmp");
		Bitmap^ west = gcnew Bitmap("../Chicken West.bmp");
		Bitmap^ north = gcnew Bitmap("../Chicken North.bmp");

		//create array of Bitmap
		array<Bitmap^>^ chickenImages = gcnew array<Bitmap^>(direction);

		//feed Bitmap to the array
		chickenImages[EDirection::EAST] = east;
		chickenImages[EDirection::SOUTH] = south;
		chickenImages[EDirection::WEST] = west;
		chickenImages[EDirection::NORTH] = north;

		//make transparent
		for (int i = 0; i < direction; i++)
		{
			Color transparentColor = chickenImages[i]->GetPixel(0, 0);
			chickenImages[i]->MakeTransparent(transparentColor);
		}

		TileMap^ bonusTileMap = tileMap;
		//return
		Bitmap^ temp = tileMap->GetMapCellBitmap(0, 0);
		int tempWidth = temp->Width;
		return gcnew Player(offScreenCanvas, 
		chickenImages, 
		framesOfChicken, 
		rand, 
		WORLD_WIDTH,
		WORLD_WIDTH,
		CHICKEN_START_POS, 
		CHICKEN_START_POS,
		7,
		bonusTileMap);
	}

	private: TileMap^ InitializeTileMap()
	{
		array<String^>^ bitmapFileNames = gcnew array<String^>(NTITLE);
		bitmapFileNames[0] = "../Flower1 Tile.bmp";
		bitmapFileNames[1] = "../Flower2 Tile.bmp";
		bitmapFileNames[2] = "../Flower3 Tile.bmp";
		bitmapFileNames[3] = "../Critter1 Tile.bmp";
		bitmapFileNames[4] = "../Critter2 Tile.bmp";
		bitmapFileNames[5] = "../Critter4 Tile.bmp";
		bitmapFileNames[6] = "../Critter5 Tile.bmp";
		bitmapFileNames[7] = "../Book Tile.bmp";
		bitmapFileNames[8] = "../Candy Tile.bmp";
		bitmapFileNames[9] = "../Potion Tile.bmp";
		bitmapFileNames[10] = "../Pumpkin Tile.bmp";
		bitmapFileNames[11] = "../Star Tile.bmp";
		bitmapFileNames[12] = "../Grass Tile.jpg";

		//create Bitmap, pass to Tile then initialize TileManager
		tileManager = gcnew TileManager(NTITLE);
		for (int i = 0; i < NTITLE; i++)
		{
			bool walkable = false;
			if (bitmapFileNames[i] == "../Grass Tile.jpg")
			{
				walkable = true;
			}
			Bitmap^ tempBitmap = gcnew Bitmap(bitmapFileNames[i]);
			Tile^ tempTile = gcnew Tile(tempBitmap, walkable);
			tileManager->SetTileArray(i, tempTile);
		}

		//initialize tileManager
		return tileMap = gcnew TileMap(tileManager, canvas, NROW, NCOL);
	}

	private: int GetSpriteSize(Player^ player)
	{
		return player->GetSpriteWidth();
	}

	private: System::Void MainForm_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
	{
		switch (e->KeyData)
		{
		case Keys::Left:
			player->spriteDirection = EDirection::WEST;
			break;
		case Keys::Up:
			player->spriteDirection = EDirection::NORTH;
			break;
		case Keys::Down:
			player->spriteDirection = EDirection::SOUTH;
			break;
		case Keys::Right:
			player->spriteDirection = EDirection::EAST;
			break;
		case Keys::Space:
			player->Shoot();
			break;
		}
	}
};
}
