#include "Player.h"

Player::Player(Graphics^ startCanvas,
	array<Bitmap^>^ startSpriteSheet,
	int startNFrames,
	Random^ startRandom,
	int startWorldWidth,
	int startWorldHeight,
	int startXpos,
	int startYpos,
	int startSpeed,
	TileMap^ startTileMap)
	: CombatSprite(startCanvas,
		startSpriteSheet,
		startNFrames,
		startRandom,
		startWorldWidth,
		startWorldHeight,
		startXpos,
		startYpos,
		startSpeed,
		startTileMap)
{
	pelletList = gcnew PelletList(tileMap, frameWidth);
}

void Player::Shoot()
{
	int bulletstartXpos = xPos + frameWidth / 2;
	int bulletstartYpos = yPos + frameWidth / 2;
	Pellet^ newPellet = gcnew Pellet(bulletstartXpos,
	bulletstartYpos, canvas, rand,
	velocityDirection[spriteDirection].X,
	velocityDirection[spriteDirection].Y);
	pelletList->addPellet(newPellet);
}

void Player::PelletSequence()
{
	pelletList->movePellets();
	//pelletList->CheckWallCollision();
	pelletList->deleteDeadPellet();
}

void Player::Move()
{
	xVel = speed;
	yVel = speed;
	int tempXposA;
	int tempYposA;
	int tempXposB;
	int tempYposB;
	int offset = 30;
	switch (spriteDirection)
	{
	case EDirection::WEST:
		tempXposA = xPos + xVel * velocityDirection[spriteDirection].X + offset;
		tempYposA = yPos + yVel * velocityDirection[spriteDirection].Y + offset;

		tempXposB = xPos + xVel * velocityDirection[spriteDirection].X + offset;
		tempYposB = yPos + yVel * velocityDirection[spriteDirection].Y - offset + frameWidth;
		break;
	case EDirection::NORTH:
		tempXposA = xPos + xVel * velocityDirection[spriteDirection].X + offset;
		tempYposA = yPos + yVel * velocityDirection[spriteDirection].Y + offset;

		tempXposB = xPos + xVel * velocityDirection[spriteDirection].X - offset + frameWidth;
		tempYposB = yPos + yVel * velocityDirection[spriteDirection].Y + offset;
		break;
	case EDirection::SOUTH:
		tempXposA = xPos + xVel * velocityDirection[spriteDirection].X + offset;
		tempYposA = yPos + yVel * velocityDirection[spriteDirection].Y + frameWidth - offset;

		tempXposB = xPos + xVel * velocityDirection[spriteDirection].X - offset + frameWidth;
		tempYposB = yPos + yVel * velocityDirection[spriteDirection].Y + frameWidth - offset;
		break;
	case EDirection::EAST:
		tempXposA = xPos + xVel * velocityDirection[spriteDirection].X + frameWidth - offset;
		tempYposA = yPos + yVel * velocityDirection[spriteDirection].Y + offset;

		tempXposB = xPos + xVel * velocityDirection[spriteDirection].X - offset + frameWidth;
		tempYposB = yPos + yVel * velocityDirection[spriteDirection].Y - offset + frameWidth;
		break;
	default:
		break;
	}

	int viewportTileXA = tempXposA / frameWidth;
	int viewportTileYA = tempYposA / frameWidth;
	int viewportTileXB = tempXposB / frameWidth;
	int viewportTileYB = tempYposB / frameWidth;

	CheckWalkable(viewportTileXA, viewportTileYA, viewportTileXB, viewportTileYB);

	if (walkable == true)
	{
		xPos = xPos + xVel * velocityDirection[spriteDirection].X;
		yPos = yPos + yVel * velocityDirection[spriteDirection].Y;
	}
}

int Player::GetSpriteWidth()
{
	return frameWidth;
}